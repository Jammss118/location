import serial #serial library for communication
import numpy as np #numpy for array and math commands
import matplotlib.pyplot as plt
from drawnow import *

x = []
y = []
ser = serial.Serial('COM7',115200, timeout=5) #Open COM7 serial to read data
plt.ion() #Telling matplotlib you want interactive mode to plot live data
counter = 0

def makeFig():
    plt.ylim(-0.2,2.1)
    plt.xlim(-0.2,2.2)
    plt.title('Tag position')
    plt.grid(True)
    plt.plot(x, y, 'ro', markersize=22) #ro means red dot
    plt2=plt.twinx()
    plt2.plot([0.0,0.0,2.02,2.02],[0.00,1.98,1.98,0.00], 'bo', markersize=5)
    plt2.axis('off')

while True:
    while(ser.inWaiting() == 0): #Wait here until there is data
        pass #If there is no data sit and loop here
    arduinoString = ser.readline() #Reading the data stream
    data = arduinoString.decode("utf-8") #converting to better format
    length = len(arduinoString) #Find the length of the incoming data
    if(length < 18): #If the data is arduino printing device connecting
        print(data)
    if(length > 18): #If the data is raw data values
        dataArray = data.split(",") #Split data into individual values
        range1 = float(dataArray[0])
        error1 = range1 - 1.41
        range2 = float(dataArray[1])
        error2 = range2 - 1.41
        range3 = float(dataArray[2])
        error3 = range3 - 1.41
        range4 = float(dataArray[3])
        error4 = range4 - 1.41
        xPos = float(dataArray[4])
        yPos = float(dataArray[5])
        print("r1: ", range1," r2: ", range2," r3: ", range3," r4: ", range4)
        print("e1: %.2f" %error1," e2: %.2f" %error2," e3: %.2f" %error3," e4: %.2f" %error4)
        print("x: ",xPos," y: ",yPos)
        x.append(xPos) #Adds each new position to x array
        y.append(yPos)
        drawnow(makeFig)
        plt.pause(.000001)
        counter = counter + 1
        if(counter > 25):
            x.pop(0)
            y.pop(0)
