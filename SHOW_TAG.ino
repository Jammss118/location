#include <SPI.h>
#include "DW1000Ranging.h"

#define READINGS 15
// connection pins
const uint8_t PIN_RST = 9; // reset pin
const uint8_t PIN_IRQ = 2; // irq pin
const uint8_t PIN_SS = SS; // spi select pin

int anchorID, devID;
float anchors[4][2] = {{0.00, 0.00},  //2
                       {0.00, 1.98},  //3 
                       {2.02, 1.98},  //4
                       {2.02, 0.00}}; //5

float x,y, xPrev, yPrev;
float distancea1[READINGS], distancea2[READINGS], distancea3[READINGS], distancea4[READINGS];
float aVals[4][2];
float aT[2][4];
float bVals[4];
float aTaVals[2][2];
float aTbVals[2];
float xTop, yTop, bot;
float ranges[4];
float xSum,ySum,dSum;
float sumXs,sumYs,sumXYD;

int d1, d2, d3, d4;
int i, i1, i2, i3, i4;
int startup = 1;

void setup() {
  Serial.begin(115200);
  delay(1000);
  DW1000Ranging.initCommunication(PIN_RST, PIN_SS, PIN_IRQ);
  DW1000Ranging.attachNewRange(newRange);
  DW1000Ranging.attachNewDevice(newDevice);
  DW1000Ranging.attachInactiveDevice(inactiveDevice);
  DW1000Ranging.startAsTag("00:01:22:EA:82:60:3B:9C", DW1000.MODE_LONGDATA_RANGE_ACCURACY, false);
  DW1000.newConfiguration();
  DW1000.setDefaults();
  DW1000.setNetworkId(1);
  DW1000.commitConfiguration();
  DW1000Ranging.useRangeFilter(true);
  Serial.println("Ranging");

}

void loop() {
  DW1000Ranging.loop();
}

void newRange(){
  anchorID = DW1000Ranging.getDistantDevice()->getShortAddress();
  if(anchorID == 2){
    ranges[0] = DW1000Ranging.getDistantDevice()->getRange();
    d1 = 1;
  }
  if(anchorID == 3){
    ranges[1] = DW1000Ranging.getDistantDevice()->getRange();
    d2 = 1;
  }
  if(anchorID == 4){
    ranges[2] = DW1000Ranging.getDistantDevice()->getRange();
    d3 = 1;
  }
  if(anchorID == 5){
    ranges[3] = DW1000Ranging.getDistantDevice()->getRange();
    d4 = 1;
  }
  
  if(d1+d2+d3+d4 == 4){
    trilaterate2D();
  }
}

void newDevice(DW1000Device* device){
  devID = device->getShortAddress();
  Serial.print("Device: "); Serial.print(devID);
  Serial.println(" added");
}

void inactiveDevice(DW1000Device* device){
  
}

void trilaterate2D(){
  for(i=0; i<4; i++){
    if(ranges[i] < 0){
      ranges[i] = ranges[i] * -1;
    }
  }
  xSum = (anchors[0][0]+anchors[1][0]+anchors[2][0]+anchors[3][0]); //move all anchor positions into one array
  ySum = (anchors[0][1]+anchors[1][1]+anchors[2][1]+anchors[3][1]);
  dSum = ranges[0] + ranges[1] + ranges[2] + ranges[3];
  sumXs = (0.25*xSum); //4 is number of anchors
  sumYs = (0.25*ySum);
  sumXYD = (0.25*(squareNum(xSum) + squareNum(ySum) - squareNum(dSum)));

  for(i=0; i<4; i++){
    aVals[i][0] = (anchors[i][0] - sumXs);
    aVals[i][1] = (anchors[i][1] - sumYs);
    aT[0][i] = aVals[i][0];
    aT[1][i] = aVals[i][1];
    bVals[i] = (squareNum(anchors[i][0]) + squareNum(anchors[i][1]) - squareNum(ranges[i]) - sumXYD)/2;
  }

  aTaVals[0][0] = (aT[0][0]*aVals[0][0])+(aT[0][1]*aVals[1][0])+(aT[0][2]*aVals[2][0])+(aT[0][3]*aVals[3][0]);
  aTaVals[0][1] = (aT[0][0]*aVals[0][1])+(aT[0][1]*aVals[1][1])+(aT[0][2]*aVals[2][1])+(aT[0][3]*aVals[3][1]);
  aTaVals[1][0] = (aT[1][0]*aVals[0][0])+(aT[1][1]*aVals[1][0])+(aT[1][2]*aVals[2][0])+(aT[1][3]*aVals[3][0]);
  aTaVals[1][1] = (aT[1][0]*aVals[0][1])+(aT[1][1]*aVals[1][1])+(aT[1][2]*aVals[2][1])+(aT[1][3]*aVals[3][1]);

  aTbVals[0] = (aT[0][0]*bVals[0])+(aT[0][1]*bVals[1])+(aT[0][2]*bVals[2])+(aT[0][3]*bVals[3]);
  aTbVals[1] = (aT[1][0]*bVals[0])+(aT[1][1]*bVals[1])+(aT[1][2]*bVals[2])+(aT[1][3]*bVals[3]);

  xTop = (aTbVals[0]*aTaVals[1][1]) - (aTbVals[1]*aTaVals[0][1]);
  bot = (aTaVals[0][0]*aTaVals[1][1]) - (aTaVals[0][1]*aTaVals[1][0]);
  yTop = (aTbVals[1]*aTaVals[0][0]) - (aTbVals[0]*aTaVals[1][0]);
  
  x = xTop / bot;
  y = yTop / bot;

 
  Serial.print(ranges[0]); Serial.print(","); Serial.print(ranges[1]); Serial.print(",");
  Serial.print(ranges[2]); Serial.print(","); Serial.print(ranges[3]); Serial.print(",");
  Serial.print(x); Serial.print(","); Serial.println(y);

  valueReset();
}

float squareNum(float number){
  return number*number;
}

void valueReset(){
  d1 = d2 = d3 = d4 = 0;
  i1 = i2 = i3 = i4 = 0;
  xPrev = x;
  yPrev = y;
  startup = 0;
}
